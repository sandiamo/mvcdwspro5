<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once("Profesor.php");
include_once("Asignatura.php");
include_once("Modelo.php");

class Modelo {

    private $fp = "Profesor.txt";
    private $fa = "Asignatura.txt";

    //**************************************
    public function getProfesores() {
        $profesores = array();

        $f = @fopen($this->fp, "r");

        if ($f) {
            $data = fgetcsv($f, 1000, ";");
            $cont = 0;
            while ($data) {
                $profesor = new Profesor($data[0], $data[1]);
                $profesores[$cont] = $profesor;
                $cont++;
                $data = fgetcsv($f, 1000, ";");
            }
            fclose($f);

            //echo "Num prof " . count($profesores) . "<br>";
            //print_r($profesores);
        } else {
            echo "Error: No se puede abrir: " . $fp . "<br>";
        }

        return $profesores;
    }

    //**************************************
    function getProfesor($profesor_) {
        $f = fopen($this->fp, "r");
        $data = fgetcsv($f, 1000, ";");
        $profesor = new Profesor("", "");

        while ($data) {
            $idp = $data[0];
            echo "Prof : " . $idp . " ==  " . $profesor_->getId() . "?<br>";
            if ($idp == $profesor_->getId()) {
                $profesor->setId($data[0]);
                $profesor->setNombre($data[1]);
                break;
            }

            $data = fgetcsv($f, 1000, ";");
        }
        fclose($f);
        //echo "Leido Objeto: " . $profesor->getId() . " " . $profesor->getNombre() . "<br>";
        return $profesor;
    }

    //**************************************
    function grabarProfesor($profesor) {

        $f = fopen($this->fp, "a");
        $linea = $profesor->getId()
          . ";" . $profesor->getNombre()
          . "\r\n";
        fwrite($f, $linea);

        fclose($f);
    }

    //**************************************

    function grabarAsignatura($asignatura) {
        $f = fopen($this->fa, "a");
        $linea = $asignatura->getId()
          . ";" . $asignatura->getNombre()
          . ";" . $asignatura->getHoras()
          . ";" . $asignatura->getProfesor()->getId()
          . "\r\n";
        fwrite($f, $linea);
        fclose($f);
    }

    //**************************************
    public function getAsignaturas() {
        $asignaturas = array();
        $f = fopen($this->fa, "r");

        if ($f) {
            $data = fgetcsv($f, 1000, ";");
            $cont = 0;
            while ($data) {
                $profesor = new Profesor($data[2], "");
                $asignatura = new Asignatura($data[0], $data[1], $data[2], $profesor);
                $data = fgetcsv($f, 1000, ";");
                $asignaturas[$cont] = $asignatura;
                $cont++;
            }
            //echo "Asignaturas " . count($asignaturas);
            fclose($f);
        } else {
            echo "Error: No se puede abrir: " . $this->fa . "<br>";
        }

        return $asignaturas;
    }

}
