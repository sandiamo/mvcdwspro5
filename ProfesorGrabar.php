
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        error_reporting(E_ALL);
        ini_set('display_errors', '1');

        include_once("Modelo.php");
        include_once("funciones.php");

        function recoge($campo) {
            if (isset($_REQUEST[$campo])) {
                $valor = htmlspecialchars(trim(strip_tags($_REQUEST[$campo])));
            } else {
                $valor = "";
            }
            return $valor;
        }

        function leer() {
            $id = recoge("id");
            $nombre = recoge("nombre");
            $profe = new Profesor($id, $nombre);
            return $profe;
        }

        //***************************
        //* Main
        //***************************

        $profesor = leer();

        if ($profesor->getId() != "" && $profesor->getNombre() != "") {

            $modelo = new Modelo();
            $modelo->grabarProfesor($profesor);

            echo "Grabado: " . $profesor->getNombre() . "<br>";
        } else {
            echo "Error: Campos vacios" . "<br>";
        }



        inicio();
        pie();
        ?>
    </body>
</html>
